package com.example.helloworld;
/* starter file for Solar System assignment -- will compile, but not execute
 * Max Langerak 12 Oct 2015
 *
 * @author Kees Huizing
 * @date 16 Oct 2012
 */

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Ellipse2D;

public class SolarSystem extends JPanel implements ActionListener, ChangeListener
{
    int FPS         = 60;          // frames per second
    double timestep = 0.5;         // simulated time that passes between frames; in days
    int delay       = 1000 / FPS;  // interval between frames; in ms
    double offsetX  = 0;
    double offsetY  = 0;
    Timer timer;
    double sizeModifier = 0.2;

    CelestialBodyExtra sun; // the center around which everything revolves

    // GUI components
    JFrame frame;
    JSlider speedSlider; // determines animations speed

    public SolarSystem() {
        //initialize everything
        this.buildGUI();
        this.buildBodies();

        //create the possibility to resize and move
        this.frame.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {}

            @Override
            public void keyPressed(KeyEvent keyEvent) {

                switch ( keyEvent.getKeyCode()) {

                    //resize with B and S, first change the offSet, this way everything you watch will stay centerd
                    // then change offset back

                    case KeyEvent.VK_B:
                        offsetX /= sizeModifier;
                        offsetY /= sizeModifier;
                        sizeModifier += 0.3;
                        offsetX *= sizeModifier;
                        offsetY *= sizeModifier;
                        break;
                    case KeyEvent.VK_S:
                        offsetX /= sizeModifier;
                        offsetY /= sizeModifier;
                        sizeModifier -= 0.3;
                        offsetX *= sizeModifier;
                        offsetY *= sizeModifier;
                        break;

                    //change the offset now
                    case KeyEvent.VK_LEFT:
                        offsetX -= 3;
                        break;
                    case KeyEvent.VK_RIGHT:
                        offsetX += 3;
                        break;
                    case KeyEvent.VK_UP:
                        offsetY -= 3;
                        break;
                    case KeyEvent.VK_DOWN:
                        offsetY += 3;
                        break;
                }
            }
            @Override
            public void keyReleased(KeyEvent keyEvent) {}
        });
        this.frame.setFocusable(true);

        this.timer = new Timer(20, this);
        this.timer.setInitialDelay(this.delay);
        this.timer.start();
    }

    // make planets and satellites and connect them
    void buildBodies( ) {
        CelestialBody mercury = new CelestialBody(0.49, Color.red, 580, 88.0);
        CelestialBody venus   = new CelestialBody(1.21, Color.red, 108, 224.7);
        CelestialBody mars    = new CelestialBody(0.68, Color.red, 228, 687.0);
        CelestialBody saturn  = new CelestialBody(12.5, Color.darkGray, 1429, 10760);
        CelestialBody uranus  = new CelestialBody(5.11, Color.blue, 2871, 30700);
        CelestialBody neptune = new CelestialBody(4.95, Color.blue, 4504, 60200);
        CelestialBody pluto   = new CelestialBody(0.2, Color.darkGray, 5913, 90600);

        CelestialBody io      = new CelestialBody(0.36, Color.yellow, 14.3 / 2 + 0.42, 1.77);
        CelestialBody europa  = new CelestialBody(0.31, Color.lightGray, 14.3 / 2 + 0.67, 3.55);
        CelestialBody ganymede = new CelestialBody(0.53, Color.gray, 14.3/2 + 1.07, 7.15);
        CelestialBody callisto = new CelestialBody(0.48, Color.gray, 14.3/2 + 1.88, 16.69);

        CelestialBody lance    = new CelestialBody(0.01, Color.white, 0.1, 5);

        CelestialBody[] appoloSats = {lance};
        CelestialBodyExtra apollo = new CelestialBodyExtra(0.02, Color.white, 0.39, appoloSats, 30);

        CelestialBody[] moonSats = {apollo};
        CelestialBodyExtra moon = new CelestialBodyExtra(0.35, Color.darkGray, 0.948, moonSats, 28);

        CelestialBody[] earthSats = {moon};
        CelestialBody earth   = new CelestialBodyExtra(1.28, Color.blue, 150, earthSats, 365.2);

        CelestialBody[] jupiterSats = {io, europa, ganymede, callisto};
        CelestialBodyExtra jupiter = new CelestialBodyExtra(14.3, Color.orange, 778, jupiterSats, 4332.0);

        CelestialBody[] sunSats = {mercury, venus, earth, mars, jupiter, saturn, uranus, neptune, pluto};

        this.sun = new CelestialBodyExtra(139, Color.yellow, 0, sunSats, 1 );
    }

    void buildGUI() {
        this.frame = new JFrame("SolarSystem");
        this.frame.add(this, BorderLayout.CENTER);

        this.speedSlider = new JSlider();
        this.frame.add(this.speedSlider, BorderLayout.SOUTH);
        this.speedSlider.addChangeListener(this);
        this.speedSlider.setFocusable(false);

        //removed buttonPanel, slider is able to bring the animation to a complete stop

        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setSize(600, 500);
        this.frame.setVisible(true);
    }

    public void paint( Graphics g ) {
        super.paintComponent(g);

        //first create a nice background
        g.setColor(Color.black);
        g.fillRect( 0, 0, getWidth(), getHeight());

        //then start drawing everything
        this.sun.draw(g, getWidth() / 2 - offsetX, getHeight() / 2 - offsetY, this.sizeModifier);
    }

    public void actionPerformed(ActionEvent e) {
        //time to make the step
        this.sun.step( this.timestep );

        //time to paint
        repaint();
    }

    // default slider runs from 0 to 100
    public void stateChanged(ChangeEvent e) {
        // get the position of the slider
        // update timestep
        this.timestep = this.speedSlider.getValue() / 20.0;
    }

    public static void main(String[] a) {
        new SolarSystem();
    }
}

// Simple celestial body, without satellites
class CelestialBody {
    //instance variables: size etc.

    Color color;
    double daysToCircle;
    double size;
    double angle = 0;
    double x;
    double y;
    double radius;

    // constructor
    public CelestialBody(double size, Color color, double radius, double daysToCircle) {
        this.size         = size;
        this.color        = color;
        this.radius       = radius;
        this.daysToCircle = daysToCircle;
    }

    // draw celestial body and trajectory
    // x, y central pivot point (position "sun")
    void draw(Graphics g, double xParent, double yParent, double sizeModifier) {
        Graphics2D g2d = (Graphics2D)g;
        g.setColor( this.color );

        //calculate next X, and Y
        this.x = ( xParent - ( (radius * sizeModifier) * Math.cos(this.angle) ));
        this.y = ( yParent + ( (radius * sizeModifier) * Math.sin(this.angle) ));

        // create circle
        Ellipse2D.Double circle = new Ellipse2D.Double(this.x - ( this.size / 2 * sizeModifier),
                this.y - (this.size / 2 * sizeModifier),
                this.size * sizeModifier, this.size * sizeModifier);

        //draw circle
        g2d.fill( circle );
    }

    // move the body over a distance corresponding to time t
    void step(double t) {
        // update angle
        this.angle += t * (Math.PI * 2) / this.daysToCircle;
    }
}

// Using inheritance a simple celestial body is extended with an array of satellities
// NB Satellites are themselves also celestial bodies, so here the subclass
// extends the superclass with an array of elements that are superclass objects.

class CelestialBodyExtra extends CelestialBody
{
    CelestialBody[] satellites;

    public CelestialBodyExtra(double size, Color color, double radius,
                               CelestialBody[] satellites, double daysToCircle) {
        super(size, color, radius, daysToCircle);
        this.satellites = satellites;
    }

    void draw(Graphics g, double x, double y, double sizeModifier) {
        // draw CelestialBody
        super.draw(g, x, y, sizeModifier);

        //draw all the sattelites
        for( CelestialBody sattelite : this.satellites ) {
            sattelite.draw(g, this.x, this.y, sizeModifier);
        }
    }

    void step(double t) {
        // similar to draw
        super.step(t);

        for( CelestialBody sattelite : this.satellites ) {
            sattelite.step(t);
        }
    }
}