package com.example.helloworld.randomartist;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

/**
 * starter file for Random Artist hoework assignment
 * 
 * code has to be added by you 
 * 
 * @author huub & kees
 */

public class Cara extends JPanel implements ActionListener {
    Random random                    = new Random();
    ArrayList<RandomShape> shapeList = new ArrayList<>();
    int windowHeight                 = 640;
    int windowWidth                  = 960;

    public Cara() {
        setPreferredSize(new Dimension(windowWidth, windowHeight)); // make panel 400 by 300 pixels
    }

    @Override
    protected void paintComponent(Graphics g) { 
        super.paintComponent(g);     // clears the background
        //gets all the shapes in the list and draws it
        for( int _i = 0; _i < shapeList.size(); _i++ ) {
            RandomShape randomShape = shapeList.get(_i);
            randomShape.draw(g);
        }
    }

    /**
     * redraws the Cara JPanel, when the button is pressed. 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        regenerate();
        repaint();
    }

    public void regenerate() {
        // clears the shapeList
        shapeList.clear();

        //generates a random amount of new polygons
        int maxAmountShapes = random.nextInt(20);
        for( int _i = 0; _i < maxAmountShapes; _i++ ) {
            shapeList.add( new RandomPolygon( windowWidth, windowHeight ));
        }
    }
 }
