package com.example.helloworld.randomartist;

import java.awt.*;

class RandomPolygon extends RandomShape {
    //random polygon
    Polygon polygon = new Polygon();
    int maxPoints   = random.nextInt(1000);

    public RandomPolygon(int maxX, int maxY) {
        super(maxX, maxY);

        //add random amount of random points to the polygon
        for( int _i = 0; _i < maxPoints; _i++ ) {
            polygon.addPoint( random.nextInt(maxX), random.nextInt(maxY) );
        }
    }

    @Override
    void draw(Graphics g) {
        //draw the polygon
        g.fillPolygon( polygon );
        g.setColor( color );
    }
}
