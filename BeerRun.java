import java.util.Scanner;

public class BeerRun {

    class Grid {
        private int[][] grid;
        private int width;
        private int height;

        Grid( int width, int height ) {
            this.width  = width;
            this.height = height;

            this.grid   = new int[width][height];
        }

        public void readNewGrid() {
            Scanner scanner = new Scanner(System.in);

            for( int _i = 0; _i < this.height; _i++ ) {
                // go through the height, read everything from width
                for( int _j = 0; _j < this.width; _j++ ) {
                    // now read the next integer into the spot
                    this.grid[_j][_i] = scanner.nextInt();
                }
            }
        }

        public int getWidth() {
            return this.width;
        }

        public int getHeight() {
            return this.height;
        }

        public int getBeer( int x, int y ) {
            return this.grid[x][y];
        }

        public void setBeer( int x, int y, int beer ) {
            this.grid[x][y] = beer;
        }

    }

    String removeFirstCharacter( String string ) {
        return string.substring(1);
    }

    int largestNumber( int a, int b, int c ) {
        int largest = 0;

        if( a >= b && a >= c ) {
            largest = a;
        } else if ( b >= a && b >= c ) {
            largest = b;
        } else if ( c >= a && c >= b ) {
            largest = c;
        }

        return largest;
    }

    int biggestInPath(Grid grid, String direction, int currentX, int currentY, int type) {

        int top;     // goes to the top part, basically NE
        int bottom;  // goes to the bottom part, basically SE
        int currentValue   = grid.getBeer(currentX, currentY);

        char nextDirection = direction.charAt(0); //next direction you're going to

        int biggest = 0; //return value

        if( type == 2 ) {
            if( direction.length() == 1 ) {
                biggest = currentValue;
            } else {
                direction = removeFirstCharacter( direction ); // removes the next character in the string
            }
        }

        if( biggest != currentValue) {
            if (nextDirection == 'E') {
                if (currentX == grid.getWidth() - 1) {
                    biggest = grid.getBeer(currentX, currentY);
                } else {
                    if (currentY == 0) {
                        top = 0;
                    } else {
                        top = biggestInPath(grid, direction, currentX + 1, currentY - 1, type);
                    }

                    if (currentY == grid.getHeight() - 1) {
                        bottom = 0;
                    } else {
                        bottom = biggestInPath(grid, direction, currentX + 1, currentY + 1, type);
                    }

                    biggest = currentValue + largestNumber(bottom, top,
                            biggestInPath(grid, direction, currentX + 1, currentY, type));
                }
            } else if (nextDirection == 'W') {
                if (currentX == 0) {
                    biggest = grid.getBeer(currentX, currentY);
                } else {
                    if (currentY == 0) {
                        top = 0;
                    } else {
                        top = biggestInPath(grid, direction, currentX - 1, currentY - 1, type);
                    }

                    if (currentY == grid.getHeight() - 1) {
                        bottom = 0;
                    } else {
                        bottom = biggestInPath(grid, direction, currentX - 1, currentY + 1, type);
                    }

                    biggest = currentValue + largestNumber(bottom, top,
                            biggestInPath(grid, direction, currentX - 1, currentY, type));
                }
            } else if (nextDirection == 'S') {
                if (currentY == grid.getHeight() - 1) {
                    biggest = grid.getBeer(currentX, currentY);
                } else {
                    if (currentX == 0) {
                        bottom = 0;
                    } else {
                        bottom = biggestInPath(grid, direction, currentX - 1, currentY + 1, type);
                    }

                    if (currentX == grid.getWidth() - 1) {
                        top = 0;
                    } else {
                        top = biggestInPath(grid, direction, currentX + 1, currentY + 1, type);
                    }

                    biggest = currentValue + largestNumber(bottom, top,
                            biggestInPath(grid, direction, currentX, currentY + 1, type));
                }
            } else if (nextDirection == 'N') {
                if (currentY == 0) {
                    biggest = grid.getBeer(currentX, currentY);
                } else {
                    if (currentX == 0) {
                        bottom = 0;
                    } else {
                        bottom = biggestInPath(grid, direction, currentX - 1, currentY - 1, type);
                    }

                    if (currentX == grid.getWidth() - 1) {
                        top = 0;
                    } else {
                        top = biggestInPath(grid, direction, currentX + 1, currentY - 1, type);
                    }

                    biggest = currentValue + largestNumber(bottom, top,
                            biggestInPath(grid, direction, currentX, currentY - 1, type));
                }

            }
        }

        return biggest;

    }

    int findMostPathPartI( Grid grid ) {
        int biggest = 0;
        int current;

        for( int _i = 0; _i < grid.getHeight(); _i++ ) {
            current = biggestInPath(grid, "E", 0, _i, 1);
            if ( current > biggest ) {
                biggest = current;
            }
        }

        return biggest;

    }

    int findMostPathPartII(Grid grid) {
        Scanner scanner     = new Scanner( System.in );

        String direction    = "";
        int directionLength = scanner.nextInt();

        for( int _i = 0; _i < directionLength; _i++ ) {
            direction += scanner.next();
        }

        int biggest = 0;
        int current = 0;

        for( int _i = 0; _i < grid.getWidth(); _i++ ) {
            for( int _j = 0; _j < grid.getHeight(); _j++ ) {
                current = biggestInPath(grid, direction, _i, _j, 2);
                if( current >= biggest ) {
                    biggest = current;
                }
            }
        }

        return biggest;

    }

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in); //defines a new scanner (DUH)
        BeerRun beerRun = new BeerRun();          //makes this a viable use of classes

        scanner.next();
        int checkType   = scanner.nextInt();
        int gridWidth   = scanner.nextInt();
        int gridHeight  = scanner.nextInt();

        Grid grid       = beerRun.new Grid( gridWidth, gridHeight ); //creates a new grid
        grid.readNewGrid();

        if( checkType == 1 ) {
            System.out.println( beerRun.findMostPathPartI(grid) );
        } else if ( checkType == 2 ) {
            System.out.println( beerRun.findMostPathPartII(grid));
        }
    }
}
