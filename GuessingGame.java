import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class GuessingGame {

    public class Code { //Code object
        private final int number;

        Code( int _number ) {
            // Code object constructor, creates code based on input
            if( _number >= 0 ) {
                number = _number;
            } else {
                Random random = new Random();
                number        = random.nextInt(100);
            }
        }

        public int getNumber() { //returns number.....
            return number;
        }
    }

    public class Attempt { //Attempt object
        private final int guess;

        Attempt( int _guess ) { //attempt constructor
            guess = _guess;
        }

        public int differsFrom( int n ) {
            // return the difference between this.guess and n
            // if ( differsFrom (n) < 0) guess is too small, else if > 0 guess too big, else correct
            return n - guess;
        }

        public int getGuess() { //returns guess
            return guess;
        }
    }

    public static void main( String args[] ) {
        GuessingGame guessingGame = new GuessingGame(); //create main Object to access Code and Attempt

        List<Attempt> attempts    = new ArrayList<>(); //make a List to save attempts
        Scanner scanner           = new Scanner(System.in);//define scanner

        System.out.println("Secretly type the code or -1 if you want me to choose");
        //generate new code
        Code code                = guessingGame.new Code(scanner.nextInt());
        System.out.println("Start guessing!");

        //game loop, only works 7 times
        for( int _i = 0; _i < 7; _i++ ) {
            //get the new attempt
            attempts.add(guessingGame.new Attempt( scanner.nextInt() ));

            //check the attempt with the requirements
            if( attempts.get(_i).differsFrom( code.getNumber() ) == 0 ) {
                //won, time to break
                System.out.println("Good guess! You won.");
                break;
            } else if( _i == 6 ) {
                // oops, too much guesses
                System.out.println("No more guesses, you lost.");
            } else if( attempts.get(_i).differsFrom(code.getNumber()) < 0 ) {
                // needs to guess higher
                System.out.println("lower");
            } else if( attempts.get(_i).differsFrom(code.getNumber()) > 0 ) {
                // needs to guess lower
                System.out.println("higher");
            }
        }

        //print out the amount of guesses
        System.out.println( attempts.size() + " guesses:" );

        //print the progressionline
        for( int _i = 0; _i < attempts.size(); _i++ ) {
            //create new string per attempt
            String progressionLine = "";
            Attempt attempt = attempts.get(_i);

            for (int _j = 0; _j <= 99; _j++) {
                //add new characters to the line, based on the characters on that place
                if (_j == attempt.getGuess()) {
                    progressionLine += "X";
                } else if (_j == code.getNumber()) {
                    progressionLine += "|";
                } else {
                    progressionLine += ".";
                }
            }

            //print progressionLine
            System.out.println(progressionLine);
        }
    }
}
