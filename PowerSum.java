public class PowerSum {

    int power(int n, int p) {
        int sum = n;
        if( p != 0) {
            for (int _i = 0; _i < p - 1; _i++) {
                sum *= n;
            }
        } else {
            sum = 1;
        }
        return sum;
    }

    int powerSum(int n) {

        int sum = 0;
        for( int _i = 0; _i <= n; _i++) {
           sum += power(2, _i);
        }
        return sum;
    }

    int fib( int n ) {
        if( n == 1 ) {
            return 1;
        } else {
            return fib(n - 1) + fib(n - 2);
        }
    }

    int powerSumRecursive(int n) {
        if( n == -1 ) {
            return 0;
        } else {
            return power(2, n) + powerSumRecursive(n - 1);
        }
    }
}
