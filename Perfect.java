public class Perfect {

    void printDivisors( int n ) {
        for( int _i = 1; _i <= ( n / 2 ); _i++ ) {
            if( n % _i == 0 ) {
                System.out.println( _i );
            }
        }
    }

    boolean isPerfect( int n ) {
        int sum = 0;

        for( int _i = 1; _i <= ( n / 2 ); _i++ ) {
            if( n % _i == 0 ) {
                sum += _i;
            }
        }

        return ( sum == n );
    }

    void allPerfect() {
        for( int _i = 1; _i <= 1000; _i++ ) {
            if( isPerfect(_i) ) {
                System.out.println(_i);
            }
        }
    }

}
