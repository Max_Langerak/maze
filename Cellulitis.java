import java.util.Scanner;

public class Cellulitis {

    void readGeneral() {
        Scanner scanner = new Scanner(System.in);
        int temporary_integer;

        String automaton_signifier = scanner.next();
        int generations_length     = scanner.nextInt();
        int generations_display    = scanner.nextInt();
        String useless_variable    = scanner.next();
        boolean[] cellRow          = new boolean[ generations_length ];

        while( scanner.hasNextInt() ) {
            temporary_integer = scanner.nextInt();
            if( !(temporary_integer > generations_length) ) {
                cellRow[(temporary_integer - 1)] = true;
            }
        }

        for( int _i = 0; _i < cellRow.length; _i++ ) {
            if( !cellRow[_i] ) {
                cellRow[_i] = false;
            }
        }

        //start with the actual generation

        draw( cellRow );
        for( int _i = 1; _i < generations_display; _i++ ) {
            System.arraycopy(getNewRow(automaton_signifier, cellRow), 0, cellRow,  0, cellRow.length );
            draw( cellRow );
        }
    }

    void readInitialConfiguration(){
    }

    void readRules() {

    }

    void draw( boolean[] cellRow ) {
        String outputLine = ""; //line to output
        for( int _i = 0; _i < cellRow.length; _i++ ) {
            if( cellRow[_i] == true ) {
                outputLine += "*";
            } else {
                outputLine += " ";
            }
        }

        System.out.println(outputLine);
    }

    boolean newCellValueByA( int cellLocation, boolean[] currentRow ) {
        boolean return_value = true;
        if( cellLocation != 0 && cellLocation != currentRow.length - 1 ) {
            if( currentRow[cellLocation] == true) {
                return_value = ((currentRow[cellLocation + 1] == true && currentRow[cellLocation - 1] == false) ||
                        (currentRow[cellLocation + 1] == false && currentRow[cellLocation - 1] == true));
            } else {
                return_value =  (currentRow[cellLocation - 1] || currentRow[cellLocation + 1]);
            }

        } else if( cellLocation == currentRow.length - 1) {
            if (currentRow[cellLocation] == true) {
                return_value =  currentRow[cellLocation - 1] == true;
            } else {
                return_value =(currentRow[cellLocation - 1]);
            }
        } else if( cellLocation == 0 ){
            if( currentRow[cellLocation] == true) {
                return_value =  currentRow[cellLocation + 1] == true;
            } else {
                return_value = (currentRow[cellLocation + 1]);
            }
        }

        return return_value;
    }

    boolean newCellValueByB( int cellLocation, boolean[] currentRow ) {
        boolean return_value = true;
        if( cellLocation != 0 && cellLocation != currentRow.length - 1) {
            if (currentRow[cellLocation]) {
                return_value = currentRow[cellLocation + 1] == false;
            } else {
                return_value = (currentRow[cellLocation + 1] == true && currentRow[cellLocation - 1] == false ||
                        (currentRow[cellLocation + 1] == false && currentRow[cellLocation - 1] == true));
            }
        } else if( cellLocation == 0 ) {
            if( currentRow[cellLocation]) {
                return_value = currentRow[cellLocation + 1] == false;
            } else {
                return_value = currentRow[cellLocation + 1] == true;
            }
        } else if( cellLocation == currentRow.length - 1) {
            if(!currentRow[cellLocation]) {
                return_value = currentRow[cellLocation - 1] == true;
            }
        }

        return return_value;
    }

    boolean newCellValueByRules( int k ) {
        boolean a = false;

        return a;
    }

    boolean[] getNewRow( String automataType, boolean[] currentRow ) {
        String checkVariable = "A";

        boolean[] newRow = new boolean[ currentRow.length ];
        for( int _i = 0; _i < currentRow.length; _i++ ) {
            if( automataType.equals("A")) {
                newRow[_i] = newCellValueByA( _i, currentRow );
            } else if ( automataType.equals("B") ) {
                newRow[_i] =newCellValueByB( _i, currentRow );
            }
        }

        return newRow;
    }

    public static void main( String args[] ) {
        Cellulitis cellulitis = new Cellulitis();
        cellulitis.readGeneral();
    }

}
