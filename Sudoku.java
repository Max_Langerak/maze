import java.util.Scanner;
public class Sudoku {
    final int SIZE = 9;     // size of the grid
    final int DMAX = 9;     // maximal digit to be filled in
    final int BOXSIZE = 3;  // size of the boxes (subgrids that should contain all digits)
    int[][] grid;     // the puzzle grid; 0 represents empty

    int solutionnr = 0; //solution counter

    // ----------------- conflict calculation --------------------

    // is there a conflict when we fill in d at position r,c?
    boolean givesConflict(int r, int  c, int d) {
        return (rowConflict(r, d) || colConflict(c, d) || boxConflict(r, c, d));
    }

    boolean rowConflict(int r, int d) {
        // checks for conflict in row r with digit d
        for( int _i = 0; _i < SIZE; _i++ ) {
            if( grid[r][_i] == d ) {
                return true;
            }
        }

        return false;
    }

    boolean colConflict(int c, int d) {
        // checks for conflict in column c with digit d
        for( int _i = 0; _i < SIZE; _i++ ) {
            if( grid[_i][c] == d ) {
                return true;
            }
        }

        return false;
    }

    boolean boxConflict(int r, int c, int d) {
        // checks for conflict in the box where row and column r, c is located with digit d
        int rowBeginning = (r / BOXSIZE) * BOXSIZE;
        int colBeginning = (c / BOXSIZE) * BOXSIZE;

        for( int _i = rowBeginning; _i < rowBeginning + BOXSIZE; _i++ ) {
            for( int _j = colBeginning; _j < colBeginning + BOXSIZE; _j++ ) {
                if( d == grid[_i][_j] ) {
                    return true;
                }
            }
        }

        return false;
    }

    // --------- solving ----------

    // finds the next empty square (in "reading order")
    // returns array of first row then column coordinate
    // if there is no empty square, returns .... (FILL IN!)
    int[] findEmptySquare() {
        //finds empty square
        int[] ret = new int[2]; //return array

        for( int _i = 0; _i < SIZE; _i++ ) {
            for( int _j = 0; _j < SIZE; _j++ ) {
                if( grid[_i][_j] == 0 ) {
                    ret[ 0 ] = _i;
                    ret[ 1 ] = _j;
                    return ret;
                }
            }
        }

        return null;
    }

    // prints all solutions that are extensions of current grid
    // leaves grid in original state
    void solve() {
        //solves the grid
        int[] emptySquare = findEmptySquare();

        if( emptySquare != null ) {
            int row = emptySquare[0];
            int col = emptySquare[1];

            for( int _i = 1; _i <= DMAX; _i++ ) {
                if( !givesConflict(row, col, _i)) {
                    //if no conflict, go recursive and solve the rest
                    grid[row][col] = _i;
                    solve();
                }
                grid[row][col] = 0;
            }

        } else {
            //we're done
            solutionnr += 1;
            print();
        }
    }

    // ------------------------- misc -------------------------

    // print the grid, 0s are printed as spaces
    void print() {
        //prints the grid
        System.out.println("+-----------+");
        for( int _i = 0; _i < SIZE; _i++ ) {
            String outputString = "|";
            for( int _j = 0; _j < SIZE; _j++ ) {
                if( grid[_i][_j] == 0 ) {
                    outputString += " ";
                } else {
                    outputString += grid[_i][_j];
                }

                if( _j == 2 || _j == 5 || _j == 8 ) {
                    outputString += "|";
                }
            }
            System.out.println(outputString);
            if( _i == 2 || _i == 5 || _i == 8 ) {
                System.out.println("+-----------+");
            }
        }
    }

    // reads the initial grid from stdin
    void read() {
        Scanner sc = new Scanner(System.in);

        for (int r=0; r<SIZE; r++) {
            if (r % BOXSIZE == 0) {
                sc.nextLine(); // read away top border of box
            }
            for (int c=0; c < SIZE; c++) {
                if (c % BOXSIZE == 0) {
                    sc.next(); // read away left border of box
                }
                String square = sc.next();
                if (".".equals(square)) {
                    grid[r][c] = 0;  // empty sqaure
                } else {
                    grid[r][c] = Integer.parseInt(square);
                }
                //System.out.print(grid[r][c]);
            }
            sc.nextLine(); // read away right border

        }
        sc.nextLine(); // read away bottom border
    }

    // --------------- where it all starts --------------------

    void solveIt() {
        read();  //read the grid
        solve(); //solve said grid

        if( solutionnr != 1 ) {
            System.out.println("Found " + solutionnr + " solutions");
        } else {
            System.out.println("Found 1 solution");
        }
    }


    public static void main(String[] args) {
        new Sudoku().solveIt();
    }
}
