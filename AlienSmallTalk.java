package com.example.helloworld;

import java.util.Scanner;

/**
 * Created by mlangerak on 29-9-15.
 */
public class AlienSmallTalk {

    Scanner scanner = new Scanner(System.in);

    // Aah variables
    String[] resources        = new String[]{ "watches", "breadsticks", "chewing gums", "stones", "sulfur" };
    String[] celestialObjects = new String[]{"comets", "asteroids", "moons", "rings" };
    String[] planets          = new String[]{"Mercurius", "Venus", "Mars", "Jupiter", "Saturnus", "Uranus", "Neptune"};
    String[] festivals        = new String[]{"the festival of Aah",
            "Beeish Christmas", "the Cee festival of the universe"};

    // Bee variables
    String[] containerType    = new String[] {"cardboard", "steel", "plastic", "ceramic"};
    String[] containerShape   = new String[] {"box", "ball", "pyramid", "dodecahedron" };


    int assignmentType; //generates global variable to get which assignment will be used

    int readNextInteger() {
        //reads and returns next integer
        return scanner.nextInt();
    }

    private String translateAahQuantity() {
        String ret = ""; //return String
        int nextInteger = readNextInteger();

        if( nextInteger == 0 ) {
            ret += "all your ";
        } else if( nextInteger == -1 ) {
            return null;
        } else {
            ret += nextInteger + " ";
        }

        return ret;
    }

    private String translateBeeContainer() {
        String ret = containerType[ readNextInteger() ] + " " + containerShape[ readNextInteger() ];
        ret       += translateBeeSentenceBuilder();

        return ret;
    }

    private String translateBeeFollowUpContainer() {
        int nextInteger = readNextInteger();

        if( nextInteger != -1 ) {
            String ret = " and a ";
            ret       += containerType[ nextInteger ] + " " + containerShape[ readNextInteger() ];
            ret       += translateBeeSentenceBuilder();

            return ret;
        } else {
            return ".";
        }
    }

    private String translateBeeSender() {
        return "";
    }

    private String translateBeeSentenceBuilder() {
        int nextInteger = readNextInteger(); //read the next integer into a variable so that we can examine it

        if( nextInteger == -1 ) {
            return ".";
        } else if( nextInteger == 5 ) {
            // another container, add it
            return " containing a " + translateBeeContainer();
        } else  {
            // there might be something following this, CHECK THIS
            return translateBeeFollowUpContainer();
        }

    }

    private String translateBee() {
        return "Build a " + translateBeeContainer(); // return string
    }

    private String translateAah() {
        String ret = ""; // return string
        int nextInteger = readNextInteger();

        //the nextInteger decides what we are going to do with the translation
        if( nextInteger == 0 ) {
            // give us shit
            ret += "Give us ";
            ret += translateAahQuantity();
            ret += resources[ readNextInteger() ];

        } else if ( nextInteger == 1 ) {
            // examine some shit
            ret += "Examine ";
            nextInteger = readNextInteger(); //write it to a temporary variable, so that we can examine it

            if( nextInteger == 0 ) {
                ret += translateAahQuantity();
                ret += celestialObjects[ readNextInteger() ] + " ";
            } else {
                ret += planets[ nextInteger - 1 ] + " ";
            }

            nextInteger = readNextInteger(); //write it to a temporary variable, so that we can examine it
            if( nextInteger != -1 ) {
                ret += "and look for " + resources[ nextInteger ];
            }

        } else {
            // Celebrate
            ret += "Celebrate ";
            ret += festivals[ readNextInteger() ];

            String temporaryString = translateAahQuantity();
            if( temporaryString != null ) {
                ret += " and offer " + temporaryString;
                ret += resources[ readNextInteger() ] + " to " + planets[ readNextInteger() ];
            }
        }

        return ret;

    }

    public static void main( String args[] ) {
        AlienSmallTalk alienSmallTalk = new AlienSmallTalk();
        Scanner scanner = new Scanner(System.in);

        scanner.next();                                    //reads away "Part", not necessary anyway
        alienSmallTalk.assignmentType = scanner.nextInt(); //gets the assignment type

        if( alienSmallTalk.assignmentType == 1 ) {
            // run number 1
            System.out.println(alienSmallTalk.translateAah());
        } else if ( alienSmallTalk.assignmentType == 2 ) {
            // run number 2
            System.out.println(alienSmallTalk.translateBee());
        } else if ( alienSmallTalk.assignmentType == 3 ) {
            // run number 3
        }
    }

}
