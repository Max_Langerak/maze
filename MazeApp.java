package com.example.helloworld;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;

public class MazeApp implements ActionListener, ChangeListener {
    //add listeners to the button, then call the method to solve it
    //add a way to draw the maze and the player in every solving method
    //to reset the player, call player.reset from maze
	
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JButton button6;
	
	Maze maze = new Maze();
	private final int NORTH = 0;
	private final int EAST  = 1;
	private final int SOUTH = 2;
	private final int WEST  = 3;

	Timer timer1 = new Timer(1,this);
	Timer timer2 = new Timer(1,this);
	Timer timer3 = new Timer(1,this);
	Timer timer4 = new Timer(1,this);
	
	MazeApp() {
		this.maze.readFromFile("/home/mlangerak/Downloads/maze.txt");
		this.maze.buildMaze();
	}
	
	@Override
	public void stateChanged(ChangeEvent changeEvent) {
		maze.randomStep();
        maze.leastVisited();
        maze.rightHand();
	}

	@Override
	public void actionPerformed(ActionEvent actionEvent) {
		//Start button
		if(actionEvent.getSource() == button1) {
			maze.repaint();
		}
		//Reset
		if (actionEvent.getSource() == button2) {
			maze.player.reset();
			maze.repaint();
		}
		//Solve1 button
		if (actionEvent.getSource() == button3) {
			timer1.start();
		}
		if (actionEvent.getSource() == timer1) {
			maze.randomStep();
			maze.repaint();
		}
		//Solve2 button
		if (actionEvent.getSource() == timer2) {
			timer2.start();
		}
		if(actionEvent.getSource() == button4) {
			maze.leastVisited();
			maze.repaint();
		}
		//Solve3 button
		if(actionEvent.getSource() == button5) {
			timer3.start();
		}
		if(actionEvent.getSource() == timer3) {
			maze.rightHand();
			maze.repaint();
		}
		//Solve4 button
		/*if(actionEvent.getSource() == button6) {
			timer4.start();
		}
		if(actionEvent.getSource() == timer4) {
			maze.shortestPath();
			maze.repaint();
		}
		*/
	}

	class Maze extends JPanel {
		int width;
		int height;
		int cellSize = 50;
		Cell[][] cells;
		int currentX;
		int currentY;
		Random random = new Random();
		Player player;
		
		//paint component
		@Override
		protected void paintComponent( Graphics g ) {
            super.paintComponent(g);

            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    cells[j][i].draw(g, j * cellSize, i * cellSize, cellSize);
                }
            }
			player.draw(g, cellSize);
		}

		void readFromFile(String filename) {
			final String WALL = "#";
			final String FLOOR = " ";
			final String START = "S";
			final String END = "E";
			File initFile;

			try {
				initFile = new File(filename);
				Scanner scanner = new Scanner(initFile, "UTF-8");
				
				width = scanner.nextInt();
				height = scanner.nextInt();
				cells = new Cell[width][height];
				scanner.nextLine(); // go to the next line, where the grid starts

				for (int y = 0; y < height; y++) {
					String line = scanner.nextLine();
					for (int x = 0; x < width; x++) {
						switch (line.substring(x, x + 1)) {
						case WALL:
							cells[x][y] = new Cell(true, false, false);
							break;
						case START:
							cells[x][y] = new Cell(false, true, false);
							player = new Player(x, y);
							break;
						case END:
							cells[x][y] = new Cell(false, false, true);
							break;
						case FLOOR:
							cells[x][y] = new Cell(false, false, false);
							break;
						}
					}
				}
			} catch (FileNotFoundException e) {
				System.out.println("Could not open file due to");
				System.out.println(e);
			}
		}

		private void buildMaze() {
			JFrame frame = new JFrame();
			frame.setBounds(100, 100, 550, 400);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(null);

			JPanel panel = new JPanel();
			panel.setBorder(new LineBorder(new Color(0, 0, 0)));
			panel.setBackground(new Color(42, 43, 44));
			panel.setBounds(6, 6, 116, 366);
			frame.getContentPane().add(panel);
			panel.setLayout(null);

			button1 = new JButton("Start");
			button1.setBounds(0, 6, 117, 40);
			panel.add(button1);
			
			button2 = new JButton("Reset");
			button2.setBounds(0, 58, 117, 40);
			panel.add(button2);
			
			button3 = new JButton("Solve 1");
			button3.setBounds(0, 110, 117, 40);
			panel.add(button3);
			
			button4 = new JButton("Solve 2");
			button4.setBounds(0, 168, 117, 40);
			panel.add(button4);
			
			button5 = new JButton("Solve 3");
			button5.setBounds(0, 220, 117, 40);
			panel.add(button5);
			
			button6 = new JButton("Solve 4");
			button6.setBounds(0, 272, 117, 40);
			panel.add(button6);

			JPanel panel2 = new JPanel();
			panel2.setBorder(new LineBorder(new Color(0, 0, 0)));
			panel2.setBackground(new Color(68, 60, 207));
			panel2.setBounds(127, 6, 417, 366);
			frame.getContentPane().add(panel2);

			frame.setVisible(true);

			panel2.add( maze );

			maze.repaint();
		}

        //Algorithm 2
        void leastVisited() {
            int leastTimesVisited = 5000;
            int directionToGoIn   = NORTH;

            //check every direction, and get the lowest amount of visited times
            //if we can go there, AND times visited is smaller than the previous least amount of times:
            // go in that direction

            if(!getCellInDirectionFromPlayer(NORTH).isWall() &&
                    getCellInDirectionFromPlayer(NORTH).getTimesVisited() < leastTimesVisited) {
                leastTimesVisited = getCellInDirectionFromPlayer(NORTH).getTimesVisited();
                directionToGoIn   = NORTH;
            }
            if(!getCellInDirectionFromPlayer(EAST).isWall() &&
                    getCellInDirectionFromPlayer(EAST).getTimesVisited() < leastTimesVisited) {
                leastTimesVisited = getCellInDirectionFromPlayer(EAST).getTimesVisited();
                directionToGoIn   = EAST;
            }
            if(!getCellInDirectionFromPlayer(SOUTH).isWall() &&
                    getCellInDirectionFromPlayer(SOUTH).getTimesVisited() < leastTimesVisited) {
                leastTimesVisited = getCellInDirectionFromPlayer(SOUTH).getTimesVisited();
                directionToGoIn   = SOUTH;
            }
            if(!getCellInDirectionFromPlayer(WEST).isWall() &&
                    getCellInDirectionFromPlayer(WEST).getTimesVisited() < leastTimesVisited) {
                directionToGoIn   = WEST;
            }

            player.goInDirection(directionToGoIn);

            Cell currentCell = cells[player.getX()][player.getY()];
            currentCell.setTimesVisited( currentCell.getTimesVisited() + 1 );

			if( cells[player.getX()][player.getY()].isEnd() ) {
				timer2.stop();
			}

		}

        //Algorithm 3
		void rightHand() {
            // first case, check whether the player can go right
            // if not able to go right, check whether the player can still go straight ahead
            // if not, try going left
            // if not just turn around
            switch (player.getDirection()) {
            case NORTH:
                if (!getCellInDirectionFromPlayer(EAST).isWall()) {
                    //can go right
                    //change direction and go in direction
                    player.goInDirection(EAST);
                } else if (!getCellInDirectionFromPlayer(NORTH).isWall()) {
                    //can still go straight ahead, let's go straight ahead ;)
                    player.goInDirection( player.getDirection() );
                } else if (!getCellInDirectionFromPlayer(WEST).isWall()) {
                    //can not go straight ahead, but can still go left
                    player.goInDirection(WEST);
                } else {
                    // can't go anywhere, turn around
                    player.goInDirection(SOUTH);
                }
                break;
            case EAST:
                if (!getCellInDirectionFromPlayer(SOUTH).isWall()) {
                    player.goInDirection(SOUTH);
                } else if (!getCellInDirectionFromPlayer(EAST).isWall()) {
                    player.goInDirection(player.getDirection());
                } else if (!getCellInDirectionFromPlayer(NORTH).isWall()) {
                    player.goInDirection(NORTH);
                } else {
                    player.goInDirection(WEST);
                }
                break;
            case SOUTH:
                if ( !getCellInDirectionFromPlayer(WEST).isWall() ) {
                    player.goInDirection(WEST);
                } else if (!getCellInDirectionFromPlayer(SOUTH).isWall()) {
                    player.goInDirection(player.getDirection());
                } else if (!getCellInDirectionFromPlayer(EAST).isWall()) {
                    player.goInDirection(EAST);
                } else {
                    player.goInDirection(NORTH);
                }
                break;
            case WEST:
                if (!getCellInDirectionFromPlayer(NORTH).isWall()) {
                    player.goInDirection(NORTH);
                } else if (!getCellInDirectionFromPlayer(WEST).isWall()) {
                    player.goInDirection(player.getDirection());
                } else if (!getCellInDirectionFromPlayer(SOUTH).isWall()) {
                    player.goInDirection(SOUTH);
                } else {
                    player.goInDirection(EAST);
                }
                break;
            }

            // keep this loop going as long as the player is not at the end

			if( cells[player.getX()][player.getY()].isEnd()) {
				timer3.stop();
			}

		}

		Cell getCellInDirectionFromPlayer(int direction) {
			//gets a cell in direction from the player
			int x = player.getX();
			int y = player.getY();

			switch( direction ) {
                case NORTH:
                    return cells[x][y - 1];
                case EAST:
                    return cells[x + 1][y];
                case SOUTH:
                    return cells[x][y + 1];
                default: //WEST
                    return cells[x - 1][y];
			}
		}

		//Algorithm 1
		void randomStep() {
            int dx[] = {1, 0, -1, 0};
            int dy[] = {0, 1, 0, -1};
            int x;
            int y;

            do {
                int i = random.nextInt(dx.length);
                x = currentX + dx[i];
                y = currentY + dy[i];
            } while (cells[x][y].isWall());

            player.setX(x);
            player.setY(y);

            currentX = x;
            currentY = y;

            if( cells[player.getX()][player.getY()].isEnd() ) {
                timer1.stop();
            }
		}
	}

	class Cell {
		private boolean isWall;
		private boolean isStart;
		private boolean isEnd;

		private int timesVisited = 0;

		Cell(boolean isWall, boolean isStart, boolean isEnd) {
			this.isWall = isWall;
			this.isStart = isStart;
			this.isEnd = isEnd;
		}
		
		//draw function
		public void draw(Graphics g, int x, int y, int cellSize) {
            //time to set different colors for everything
            if( this.isWall() ) {
                g.setColor(Color.BLACK);
            } else if ( this.isStart() ) {
                g.setColor(Color.GREEN);
            } else if (this.isEnd()) {
                g.setColor( Color.RED );
            } else {
                g.setColor( Color.WHITE );
            }
            g.fillRect(x, y, cellSize, cellSize);
		}

		public boolean isWall() {
			return isWall;
		}

		public boolean isEnd() {
			return isEnd;
		}

		public boolean isStart() {
			return isStart;
		}

		public void setTimesVisited(int timesVisited) {
			this.timesVisited = timesVisited;
		}

		public int getTimesVisited() {
			return timesVisited;
		}
	}

	class Player {
		private int x;
		private int y;
        private final int originalX;
        private final int originalY;
		private int direction = 0; // N - 0, E - 1, S - 2, W - 3

		Player(int x, int y) {
			this.x = x;
			this.y = y;

            this.originalX = x;
            this.originalY = y;
		}

        public void draw( Graphics g, int cellSize ) {
            g.setColor(Color.CYAN);
            g.fillRect(this.x * cellSize, this.y * cellSize, cellSize, cellSize);
        }

		public void goInDirection(int direction) {
			this.direction = direction;

			switch (this.direction) {
			case NORTH:
				this.y--;
				break;
			case EAST:
				this.x++;
				break;
			case SOUTH:
				this.y++;
				break;
			case WEST:
				this.x--;
				break;
			}
		}

		public int getDirection() {
			return direction;
		}

		public void setX(int x) {
			this.x = x;
		}

		public void setY(int y) {
			this.y = y;
		}

		public int getX() {
			return x;
		}

		public int getY() {
			return y;
		}

        public void reset() {
            this.x = this.originalX;
            this.y = this.originalY;
        }
	}

	public static void main(String[] args) {
		new MazeApp();
	}
}